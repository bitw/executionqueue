<?php

interface ExecutionQueueInterface
{
    public function put(string $key, $value);

    public function get(string $key);
}