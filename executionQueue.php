<?php
/**
 * JSmart CMS
 * ===========================================================================
 * @author Vadim Shestakov
 * ---------------------------------------------------------------------------
 * @link https://jsmart.ru/
 * ---------------------------------------------------------------------------
 * @license https://jsmart.ru/cms/eula
 * ---------------------------------------------------------------------------
 * @copyright 2020 Vadim Shestakov
 * ===========================================================================
 */
class ExecutionQueue
{
    protected $queue        = [];
    protected $handler      = '';
    protected $counterQueue = '';
    protected $counterRow   = '';

    /**
     * __construct
     *
     * @param string $handler
     * @return void
     */
    public function __construct($handler = 'BaseHandler')
    {
        $this->handler = new $handler();

        $this->counterQueue = (int) $this->handler->get('counterQueue');
        $this->counterRow   = (int) $this->handler->get('counterRow');
    }

    /**
     * Add Queue
     *
     * @param mixed $class - class name or function
     * @param mixed $data
     * @param int $limit
     * @return void
     */
    public function add($class, $data = null, $limit = 1)
    {
        $this->queue[] = [
            'class' => $class,
            'data'  => $data,
            'limit' => $limit
        ];
    }

    /**
     * Run
     *
     * @return mixed - boolean or string process
     */
    public function run()
    {
        $counterQueue = 0;

        foreach ($this->queue as $key => $queue)
        {
            $counterQueue++;

            if ($counterQueue <= $this->counterQueue) {
                continue;
            }

            return $this->callQueue($key, $queue['limit']);
        }

        $this->clean();

        return TRUE;
    }

    /**
     * callQueue
     *
     * @param int $key
     * @param int $limit
     * @return string - process
     */
    public function callQueue($key, $limit = 1)
    {
        $counterRow     = 0;
        $counterCall    = 0;

        foreach ($this->queue[$key]['data'] as $dataQueue)
        {
            $counterRow++;

            if ($counterRow <= $this->counterRow) {
                continue;
            }

            $callQueue = $this->queue[$key]['class'];

            if (!is_callable($callQueue)) {
                $callQueue = new $callQueue;
            }

            $callQueue($dataQueue);

            $this->handler->put('counterRow', $counterRow);

            $counterCall++;

            if (count($this->queue[$key]['data']) > $this->counterRow + $counterCall) {
                if ($counterCall >= $limit) {
                    return 'process';
                }
            }
        }

        $this->handler->put('counterQueue', $this->counterQueue + 1);
        $this->handler->put('counterRow', 0);

        return 'process';
    }

    /**
     * Запрос строк
     *
     * @param array $where - условия выборки
     * @return array - данные
     */
    public function clean()
    {
        $this->handler->put('counterQueue', 0);
        $this->handler->put('counterRow', 0);
    }
}